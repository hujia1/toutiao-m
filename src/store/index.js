import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage'
Vue.use(Vuex)

const TOKEN_KEY = 'TOUTIAO_USER'

export default new Vuex.Store({
  state: {
    // 用户的登录状态信息
    // user: JSON.parse(window.localStorage.getItem('TOUTIAO_USER'))
    user: getItem(TOKEN_KEY)
  },
  mutations: {
    setUser(state, data) {
      // store存储
      state.user = data
      // window.localStorage.setItem('TOUTIAO_USER', JSON.stringify(user))
      // 本地存储
      setItem(TOKEN_KEY, state.user)
    }
  },
  getters: {
    userAvatar(state) {
      // 默认的头像地址
      let imgSrc = 'https://img.yzcdn.cn/vant/cat.jpeg'
      // 如果用户信息对象中包含 photo 属性的值，则为 imgSrc 重新赋值
      if (state.user.photo) {
        imgSrc = state.user.photo
      }

      return imgSrc
    }
  },
  actions: {},
  modules: {}
})
