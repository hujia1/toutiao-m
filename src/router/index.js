import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import { Dialog } from 'vant'

Vue.use(VueRouter)

// 路由表
const routes = [
  {
    path: '/login',
    name: 'login',
    meta: { requiresAuth: false },
    component: () => import('@/views/login')
  },
  {
    path: '/',
    // name: 'layout', // 如果父路由有默认子路由，那它的 name 没有意义
    component: () => import('@/views/layout'),
    children: [
      {
        path: '', // 默认子路由，只能有1个
        name: 'home',
        meta: { requiresAuth: false },
        component: () => import('@/views/home')
      },
      {
        path: '/qa',
        name: 'qa',
        meta: { requiresAuth: false },
        component: () => import('@/views/qa')
      },
      {
        path: '/video',
        name: 'video',
        meta: { requiresAuth: false },
        component: () => import('@/views/video')
      },
      {
        path: '/my',
        name: 'my',
        meta: { requiresAuth: false },
        component: () => import('@/views/my')
      }
    ]
  },
  {
    path: '/search',
    meta: { requiresAuth: false },
    component: () => import('@/views/search')
  },
  {
    path: '/article/:articleId',
    name: 'article',
    component: () => import('@/views/article'),
    meta: { requiresAuth: false },
    // 将路由动态参数映射到组件的 props 中，更推荐这种做法
    props: true
  },
  {
    path: '/user/:userId',
    name: 'user',
    component: () => import('@/views/user'),
    meta: { requiresAuth: true },
    props: true
  },
  {
    path: '/chat',
    name: 'chat',
    component: () => import('@/views/Chat'),
    meta: { requiresAuth: true }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // 如果要到的路由是login登录页面 或 要去的路由中requiresAuth属性不为true的放行
  // 定义路由中requiresAuth属性 用来做判断
  if (to.name === 'login' || !to.meta.requiresAuth) {
    return next(0)
  }

  // 如果vuex中的user有数据 已登录 那么放行
  if (store.state.user) {
    return next()
  }

  Dialog.confirm({
    title: '该功能需要登录，确认登录吗？'
  })
    .then(() => {
      next({
        name: 'login',
        query: {
          redirect: from.fullPath
        }
      })
    })
    .catch(() => {
      // on cancel
    })
})

export default router
