/**
 * 请求模块
 */
import axios from 'axios'
import store from '@/store'
import JSONBig from 'json-bigint'
import { Toast } from 'vant'
// import 'vant/es/toast/style'
import router from '@/router'
const request = axios.create({
  // 接口的基准路径
  baseURL: 'http://www.liulongbin.top:8000',

  // 自定义后端返回的原始数据
  // data: 后端返回的原始数据，说白了就是 JSON 格式的字符串
  transformResponse: [
    function (data) {
      try {
        return JSONBig.parse(data)
      } catch (err) {
        return data
      }

      // axios 默认会在内部这样来处理后端返回的数据
      // return JSON.parse(data)
    }
  ]
})
// 定义一个用来重新请求的axions 不能使用当前的request 因为实在当前request中使用的
const refreshtoken = axios.create({
  // 接口的基准路径
  baseURL: 'http://www.liulongbin.top:8000'
})

// 请求拦截器
// Add a request interceptor
request.interceptors.request.use(
  function (config) {
    // 请求发起会经过这里
    // config：本次请求的请求配置对象
    const { user } = store.state
    if (user && user.token) {
      config.headers.Authorization = `Bearer ${user.token}`
    }

    // 注意：这里务必要返回 config 配置对象，否则请求就停在这里出不去了
    return config
  },
  function (error) {
    // 如果请求出错了（还没有发出去）会进入这里
    return Promise.reject(error)
  }
)

// 响应拦截器
// Add a response interceptor
request.interceptors.response.use(
  // 在2xx范围内的任何状态代码都会触发此函数，这里主要用于处理响应数据
  (response) => {
    // 响应成功进入第1个函数
    // 该函数的参数是响应对象
    return response
  },
  // 任何超出2xx范围的状态码都会触发此函数，这里主要用于处理响应错误
  // 响应失败进入第2个函数，该函数的参数是错误对象
  async (error) => {
    // 如果响应码是 401 ，则请求获取新的 token
    // 响应拦截器中的 error 就是那个响应的错误对象
    console.dir(error)

    // 从错误对象中解构出状态码
    const { status } = error.response
    const { config } = error

    if (error.response && status === 401) {
      // 未授权   // 校验是否有 refresh_token
      const user = store.state.user
      if (!user || !user.refresh_token) {
        // router.push('/login')
        // return redirectLogin()
        return redirectLogin()
      }

      // 如果有refresh_token，则请求获取新的 token
      try {
        const res = await refreshtoken({
          method: 'PUT',
          url: '/v1_0/authorizations',
          headers: {
            Authorization: `Bearer ${user.refresh_token}`
          }
        })

        // 如果获取成功，则把新的 token 更新到容器中
        console.log('刷新 token  成功', res)
        store.commit('setUser', {
          token: res.data.data.token, // 最新获取的可用 token
          refresh_token: user.refresh_token // 还是原来的 refresh_token
        })

        // 把之前失败的用户请求继续发出去
        // config 是一个对象，其中包含本次失败请求相关的那些配置信息，例如 url、method 都有
        // return 把 request 的请求结果继续返回给发请求的具体位置
        return request(config)
      } catch (error) {
        // 如果获取失败，直接跳转 登录页
        console.log('请求刷线 token 失败', error)
        // router.push('/login')
        redirectLogin()
      }
    } else if (status === 403) {
      // 没有权限
    } else if (status === 404) {
      // 资源不存在
      Toast.fail({
        message: '请求资源不存在',
        forbidClick: true
      })
    } else if (status >= 500) {
      // 服务端异常
      Toast.fail({
        message: '服务端异常，请稍后重试',
        forbidClick: true
      })
    }

    // 将未处理的异常往外抛
    return Promise.reject(error)
  }
)
function redirectLogin() {
  // router.currentRoute 当前路由对象，和你在组件中访问的 this.$route 是同一个东西
  // query 参数的数据格式就是：?key=value&key=value
  // 做路由跳转登录时  需要给路由加上参数
  router.push('/login?redirect=' + router.currentRoute.fullPath)
}

export default request
