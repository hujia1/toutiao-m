export const getItem = (name) => {
  const data = window.window.localStorage.getItem(name)
  try {
    //   从本地获取到的字符串结果进行json对象转换 如果不能转换则报错
    return JSON.parse(data)
  } catch (err) {
    return data
  }
}

export const setItem = (name, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(name, value)
}

export const removeItem = (name) => {
  window.localStorage.removeItem(name)
}
