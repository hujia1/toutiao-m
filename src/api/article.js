/**
 * 文章请求模块
 */
import request from '@/utils/request'

// 根据频道 Id 获取文章列表数据
export const getArticles = (obj) => {
  return request({
    method: 'GET',
    url: '/v1_0/articles',
    params: {
      channel_id: obj.channel_id,
      timestamp: obj.timestamp
    }
  })
}

/**
 * 添加用户频道
 */
export const addUserChannel = (channels) => {
  return request({
    method: 'PUT',
    url: '/v1_0/user/channels',
    data: {
      channels: [channels]
    }
  })
}

/**
 * 根据 id 获取指定文章
 */
export const getArticleById = (articleId) => {
  return request({
    method: 'GET',
    url: `/v1_0/articles/${articleId}`
  })
}

/**
 * 收藏文章
 */
export const addCollect = (target) => {
  return request({
    method: 'POST',
    url: '/app/v1_0/article/collections',
    data: {
      target
    }
  })
}

/**
 * 取消收藏文章
 */
export const deleteCollect = (target) => {
  return request({
    method: 'DELETE',
    url: `/app/v1_0/article/collections/${target}`
  })
}

/**
 * 点赞
 */
export const addLike = (articleId) => {
  return request({
    method: 'POST',
    url: '/v1_0/article/likings',
    data: {
      target: articleId
    }
  })
}
/**
 * 取消 点赞
 */
export const deleteLike = (articleId) => {
  return request({
    method: 'DELETE',
    url: `/v1_0/article/likings/${articleId}`
  })
}
