module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['plugin:vue/essential', '@vue/standard'],
    parserOptions: {
        parser: 'babel-eslint'
    },
    rules: {
        indent: ['off', 2], // 缩进
        'eol-last': 0, // 末尾关闭换行检测
        'space-before-function-paren': 0, // 函数名和括号之间的空格
        'vue/no-unused-components': 'off', // 函数名定义未使用的报错关闭
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
    }
}