/**
 * 配置文档：https://cli.vuejs.org/zh/config/
 */

module.exports = {
  /**
   * / 用于部署在 HTTP 服务中
   * 如果是混合应用，则将其设置为相对路径 ./
   * 参考文档：https://cli.vuejs.org/zh/config/#publicpath
   */
  publicPath: './',

  devServer: {
    // host: '和手机在一个网段的网卡地址'
    host: '192.168.0.117'
  }
}
